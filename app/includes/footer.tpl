<section class="pre-footer">
  <div class="container">
    <p>
      wallet.egem.io does not hold your keys for you. We cannot access accounts, recover keys, reset passwords, nor reverse transactions. Protect your keys &amp; always check that you are on correct URL.
      <a role="link" tabindex="0" data-toggle="modal" data-target="#disclaimerModal">
        You are responsible for your security.
      </a>
    </p>
  </div>
</section>

<footer class="footer" role="content" aria-label="footer" ng-controller='footerCtrl' >

  <article class="block__wrap" style="max-width: 1780px; margin: auto;">

    <section class="disclaimer">

      <a href="/"><img src="images/logo-myetherwallet.png" height="45px" width="auto" alt="Egem Wallet" class="footer--logo"/></a>

      <p>
        <span translate="FOOTER_1">
          Free, open-source, client-side interface for generating Egem wallets &amp; more. Interact with the Egem blockchain easily &amp; securely. Double-check the URL ( .com ) before unlocking your wallet.
        </span>
      </p>

      <p>
        <a aria-label="knowledge base" href="https://myetherwallet.github.io/knowledge-base/" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
          Knowledge Base
        </a>
      </p>

      <p>
        <a data-target="#disclaimerModal" data-toggle="modal" target="_blank" rel="noopener noreferrer" role="link" tabindex="0"  translate="FOOTER_4">
          Disclaimer
        </a>
      </p>

      <p ng-show="showBlocks">
        Latest Block#: {{currentBlockNumber}}
      </p>

      <p>
        &copy; 2018 Egem
      </p>

    </section>

    <section>
        <h5 ng-hide="curLang=='en'">
          <i>🏅</i>
          <span translate="Translator_Desc"> Thank you to our translators </span>
        </h5>
        <p ng-hide="curLang=='en'">
          <span translate="TranslatorName_1"></span>
          <span translate="TranslatorName_2"></span>
          <span translate="TranslatorName_3"></span>
          <span translate="TranslatorName_4"></span>
          <span translate="TranslatorName_5"></span>
        </p>

        <div>
          <strong class="footer-col-title">Tools</strong>
          <ul>
            <li>
              <a href="https://wallet.egem.io" target="_blank" rel="noopener" role="link" tabindex="0">
                wallet.egem.io
              </a>
            </li>
            <li>
              <a href="https://stats.egem.io" target="_blank" rel="noopener" role="link" tabindex="0">
                Egem Stats
              </a>
            </li>
            <li>
              <a href="https://wallet.egem.io/helpers.html" target="_blank" rel="noopener" role="link" tabindex="0">
                Unit Converters &amp; ENS Debugging
              </a>
            </li>
            <li>
              <a href="https://wallet.egem.io/signmsg.html" target="_blank" rel="noopener" role="link" tabindex="0">
                Sign Message
              </a>
            </li>
            <li>
              <a href="https://pool.egem.io" target="_blank" rel="noopener" role="link" tabindex="0">
                Pools
              </a>
            </li>
            <li>
              <a aria-label="my ether wallet chrome extension" href="https://chrome.google.com/webstore/detail/myetherwallet-cx/nlbmnnijcnlegkjjpcfjclmcfggfefdm?hl=en" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
                MyEtherWallet Chrome Extension
              </a>
            </li>
            <li>
              <a aria-label="Anti-Phishing chrome extension" href="https://chrome.google.com/webstore/detail/etheraddresslookup/pdknmigbbbhmllnmgdfalmedcmcefdfn" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
                EAL "Don't Get Phish'd" Chrome Extension
              </a>
            </li>
          </ul>
        </div>
    </section>
    <section>
      <div>
          <strong class="footer-col-title">Links</strong>
          <ul>
            <li>
              <a aria-label="egem website URL" href="https://egem.io" target="_blank" rel="noopener" role="link" tabindex="0">
                Egem Website
              </a>
            </li>
            <li>
              <a aria-label="my egem wallet gitlab" href="https://git.egem.io/community/MyEgemWallet" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
                Gitlab: Current Site &amp; CX
              </a>
            </li>
            <li>
              <a aria-label="egem FAQ" href="https://egem.io/en/faq" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
                Egem FAQ
              </a>
            </li>
            <li>
              <a aria-label="egem docs" href="https://docs.egem.io" target="_blank" rel="noopener noreferrer" role="link" tabindex="0">
                Egem Docs
              </a>
            </li>
          </ul>
      </div>
    </section>
    <section>
      <strong class="footer-col-title">Social</strong>
      <ul>
        <li>
          <a href="https://discord.gg/z4faFxD" target="_blank">Discord</a>
        </li>
        <li>
          <a href="https://twitter.com/EgemOfficial" target="_blank">Twitter</a>
        </li>
        <li>
          <a href="https://www.reddit.com/r/egem" target="_blank">Reddit</a>
        </li>
        <li>
          <a href="https://git.egem.io" target="_blank">Gitlab</a>
        </li>
        <li>
          <a href="https://bitcointalk.org/index.php?topic=2120193" target="_blank">BitcoinTalk ANN</a>
        </li>
        <li>
          <a href="https://forum.egem.io" target="_blank">Community Forums</a>
        </li>
      </ul>
    </section>
  </article>

</div>


</footer>

@@if (site === 'mew' ) { @@include( './footer-disclaimer-modal.tpl',   { "site": "mew" } ) }
@@if (site === 'cx'  ) { @@include( './footer-disclaimer-modal.tpl',   { "site": "cx"  } ) }

@@if (site === 'mew' ) { @@include( './onboardingModal.tpl',   { "site": "mew" } ) }
@@if (site === 'cx'  ) { @@include( './onboardingModal.tpl',   { "site": "cx"  } ) }


</main>
</body>
</html>
